# ANESE analysis


* Du hast Projekt https://github.com/daniel5151/ANESE.git gewählt.

* Schaue, welche Dependencies das Projekt hat.

    Die Projekt hat die folgende Dependencies:

    * cfg_path
    * clara 
    * cute_headers
    * miniz
    * sdl2
    * SDL_inprint
    * simpleini
    * stb


* Wähle eine von diesen Dependencies, nennen wir sie "lib_xyz"
* Stelle sicher (check or enforce), dass lib_xyz *statisch* gelinkt wird

    Die SimpleINI Bibliothek wird ausgewählt. 
    In der Datei CMakeLists.txt findet sich folgende Linie:

    ```
    # ---- SimpleINI ---- #
    include_directories(thirdparty/SimpleINI)
    add_library(SimpleINI STATIC thirdparty/SimpleINI/ConvertUTF.c)
    ```

    `add_library(<name> STATIC)` bedeutet, dass die SimpleINI-Bibliothek Datei statisch gelinkt wird und das _source_ Datei ist `ConvertUTF.c`.

    Danach ist die Bibliothek gelinkt:

    ```
    target_link_libraries(anese
      ${SDL2_LIBRARY}
      SDL_inprint
      SimpleINI
      miniz
    )
    ```


* Stelle fest (identify) welche Version von lib_xyz es ist, sagen wir es ist Version 1.2.3

    Die Bibliotheksversion ist 4.17, wie aus man in der Datei SimpleINI.h lesen kann:

    ```
    /** @mainpage

        <table>
            <tr><th>Library     <td>SimpleIni
            <tr><th>File        <td>SimpleIni.h
            <tr><th>Author      <td>Brodie Thiesfield [code at jellycan dot com]
            <tr><th>Source      <td>https://github.com/brofield/simpleini
            <tr><th>Version     <td>4.17
        </table>
    ```

* Baue das Projekt
* Identifiziere eine gebaute (compiled) Datei, die lib_xyz in der Version 1.2.3 statisch gelinkt hat.

    Die statisch gelinkte SimpleINI Bibliothek Datei befindet sich im _[build](build)_ und heißt `libSimpleINI.a`. 

    Innerhalb der Bibliothek Datei befindet sich die `ConvertUTF.c.o` Objekt Datei, die mit `add_library(SimpleINI STATIC thirdparty/SimpleINI/ConvertUTF.c)` deklariert wurde.

    In der gebaute Datei kann mit `gdb -q -ex 'info sources' -ex 'quit' anese | grep 'SimpleINI'` festgestellt werden, dass das gebaute Projekt eine Datei aus der SimpleINI Bibliothek enthält.

    ```
    $ build: gdb -q -ex 'info sources' -ex 'quit' anese | grep 'SimpleINI' 
    /home/emanuelacm/Documentos/anese/anese-analysis/ANESE/thirdparty/SimpleINI/SimpleIni.h, 
    ```

* Schicke mir die Datei zusammen mit der Information: "lib_xyz, Version 1.2.3"

    * SimpleINI Bibliothek Datei: [libSimpleINI.a](libSimpleINI.a)

    * gefunden Datei in gebaute Projekt: [SimpleIni.h](SimpleIni.h)



